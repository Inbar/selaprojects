package com.example.inbar.race;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import java.util.Random;
import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {

    private final Thread racePlayer[] = new Thread[6];
    private Drawing horse[] = new Drawing[6];
    private ImageView track[] = new ImageView[6];
    private int placment = 1;
    private int numberOfHorses = 6;
    private Handler callback;
    private Message msg[] = new Message[6];
    private AlertDialog.Builder alertResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //draw the start of the race
        drawStartRace();
        createThreads();

        //set the result dialog
        setDialog();

        //set start button
        Button startRace = (Button) findViewById(R.id.start_button);
        startRace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //enable the start button
                view.setEnabled(false);

                //get number of horses
                EditText num = (EditText) findViewById(R.id.num);
                numberOfHorses = Integer.valueOf(num.getText().toString());
                if (numberOfHorses < 0 || numberOfHorses > 6) {
                    numberOfHorses = 6;
                }

                //start race
                for (int i = 0; i < numberOfHorses; i++) {
                    racePlayer[i].start();
                }
            }
        });

        //set handler for threads
        callback = new Handler(new Handler.Callback() {

            @Override
            public boolean handleMessage(Message msg) {

                //if it's the last thread
                if(msg.arg1 == numberOfHorses) {
                    StringBuilder result = new StringBuilder();
                    for(int i = 0; i < numberOfHorses; i++) {
                        result.append("Horse ");
                        result.append(i);
                        result.append(": ");
                        result.append(horse[i].getPlacement());
                        result.append("\n");
                    }

                    alertResult.setMessage(result.toString());
                    alertResult.show();

                    //set new race
                    for(int i = 0; i < numberOfHorses; i++) {
                        horse[i].setNewRace();
                        track[i].setImageResource(0);
                        track[i].setImageDrawable(horse[i]);
                    }
                    createThreads();
                    setDialog();

                    //active start button
                    Button startRace = (Button) findViewById(R.id.start_button);
                    startRace.setEnabled(true);

                    return true;
                }
                return false;
            }
        });
    }

    private void setDialog() {
        alertResult = new AlertDialog.Builder(MainActivity.this);
        alertResult.setTitle("Result");
        alertResult.setPositiveButton(android.R.string.yes, null);
        alertResult.setIcon(android.R.drawable.ic_dialog_alert);
    }


    private void createThreads() {

        int[] range = {0,1,2,3,4,5};

        for(final int i : range) {
            racePlayer[i] = new Thread (){
                public void run() {
                    int sleepTime;
                    Random generator=new Random();
                    try {
                        while(!horse[i].finish()) {
                            horse[i].moveHours();
                            track[i].setImageResource(0);
                            track[i].setImageDrawable(horse[i]);
                            sleepTime = generator.nextInt(101) + 50;
                            sleep(sleepTime);
                        }

                        //synchronizing the var
                        int place;
                        synchronized(this)
                        {
                            place = placment;
                            placment++;
                        }
                        horse[i].setPlacement(place);

                        //end of match - callback
                        msg[i] = new Message();
                        msg[i].arg1 = place;
                        callback.sendMessage(msg[i]);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
        }
    }


    private void drawStartRace() {

        String buttonID;
        int resID;
        for(int i = 0; i < 6; i++) {
            horse[i] = new Drawing();
            buttonID = "imageView" + i;
            resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            track[i] = findViewById(resID);
            track[i].setImageDrawable(horse[i]);
        }
    }
}
