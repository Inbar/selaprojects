package com.example.inbar.race;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

public class Drawing extends Drawable {
    private final Paint redPaint;
    private int step;
    private int placement;

    public Drawing() {
        // set up color and text size
        redPaint = new Paint();
        redPaint.setARGB(255, 255, 0, 0);
        step = 0;
    }

    public void setNewRace() {
        placement = 0;
        step = 0;
    }

    public int getPlacement() {
        return placement;
    }

    public void setPlacement(int getPlacement) {
        this.placement = getPlacement;
    }

    public void moveHours() {
        this.step += 10;
    }

    public boolean finish() {
        return (getBounds().height() - step <= 0);
    }

    @Override
    public void draw(Canvas canvas) {
        // get the drawable's bounds
        int width = getBounds().width();
        int height = getBounds().height();
        float radius = Math.min(width, height) / 4;

        // draw a red circle
        canvas.drawCircle(width/2, height - step, radius, redPaint);
    }

    @Override
    public void setAlpha(int alpha) { }

    @Override
    public void setColorFilter(ColorFilter colorFilter) { }

    @Override
    public int getOpacity() {
        return PixelFormat.OPAQUE;
    }


}
