package com.example.inbar.phonebook;

import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.view.View;
import android.view.KeyEvent;
import java.io.File;
import java.util.Map;
import java.util.TreeMap;

public class MainActivity extends AppCompatActivity {

    PhoneBook book;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        File bookFile = new File("phoneBook.txt");
        book = new PhoneBook(this, bookFile);

        createBookLay();

        //add new number
        final FloatingActionButton addButton = findViewById(R.id.add_number);
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(v.getContext());
                alert.setTitle("Add number");

                //create layout to get 2 inputs
                LinearLayout layout = new LinearLayout(v.getContext());
                layout.setOrientation(LinearLayout.VERTICAL);

                final EditText name = new EditText(v.getContext());
                name.setHint("Name");
                final EditText phone = new EditText(v.getContext());
                phone.setHint("Phone number");
                layout.addView(name);
                layout.addView(phone);

                alert.setView(layout);
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String newName = name.getText().toString();
                        String newPhone = phone.getText().toString();
                        book.addNewNumber(newName, newPhone);

                    }
                });
                alert.setNegativeButton("Cancel", null);
                alert.show();
            }
        });

        //save the phone book to file
        final FloatingActionButton saveButton = findViewById(R.id.save_id);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                book.writeBookToFile();
            }
        });

        //load the phone book from file
        final FloatingActionButton loadButton = findViewById(R.id.load_id);
        loadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                book.reloadBookFromFile();
                cleanList();
                createBookLay();
            }
        });

        //search func
        final EditText edittext = (EditText) findViewById(R.id.searchB);
        edittext.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // when press enter
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    //search for number
                    String name = edittext.getText().toString();
                    String phone = book.searchNum(name);
                    String message = "not found.";
                    if(phone.length() > 0) {
                        message = "number: " + phone;
                    }

                    edittext.setText("");

                    new AlertDialog.Builder(v.getContext())
                            .setTitle(name)
                            .setMessage(message)
                            .setPositiveButton(android.R.string.yes, null)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();

                    return true;
                }
                return false;
            }
        });
    }

    private void cleanList() {
        LinearLayout bookLayout = (LinearLayout) findViewById(R.id.bookLayout);
        bookLayout.removeAllViews();
    }

    private void createBookLay() {

        LinearLayout bookLayout = (LinearLayout) findViewById(R.id.bookLayout);

        TreeMap<String, String> treeChar;

        //for each alphabet
        for(char i = 'a'; i <= 'z'; i++) {
            treeChar = book.getBookByAlphabet(i);

            if(treeChar == null) {
                continue;
            }

            //create table
            TableLayout table = new TableLayout(this);
            int resourceId = this.getResources().getIdentifier(String.valueOf(i), "id", this.getPackageName());
            table.setId(resourceId);

            //create row title
            TableRow newRow = new TableRow(this);
            TextView name = new TextView(this);
            //title
            name.setText(String.valueOf(i) + " letter:");
            newRow.addView(name);
            table.addView(newRow);

            //for each number
            for(Map.Entry<String, String> j : treeChar.entrySet()) {

                //create row
                final TableRow newRow2 = new TableRow(this);

                //create person info
                final TextView name2 = new TextView(this);
                final EditText phone = new EditText(this);
                final Button del = new Button(this);

                phone.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        // when press enter
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                                (keyCode == KeyEvent.KEYCODE_ENTER)) {

                            //update the number
                            book.updateNumber(name2.getText().toString(), phone.getText().toString());
                            return true;
                        }
                        return false;
                    }
                });
                del.setText("delete");
                del.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {

                        book.deleteNumber(String.valueOf(name2.getText()));

                        newRow2.removeView(name2);
                        newRow2.removeView(phone);
                        newRow2.removeView(del);

                    }
                });


                name2.setText(j.getKey());
                phone.setText(j.getValue());

                //add to row
                newRow2.addView(name2);
                newRow2.addView(phone);
                newRow2.addView(del);

                //add to table
                table.addView(newRow2);
            }
            //add table to layout
            bookLayout.addView(table);

        }
    }
}
