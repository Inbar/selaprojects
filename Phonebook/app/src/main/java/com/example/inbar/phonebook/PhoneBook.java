package com.example.inbar.phonebook;

import android.content.Context;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class PhoneBook {

    private HashMap<String, TreeMap<String, String>> orderBooks;
    private File bookFile;
    private Context fileContext;
    private boolean changeFlag = false;

    public PhoneBook (Context fileContext, File bookFile) {
        this.bookFile = bookFile;
        this.fileContext = fileContext;
        //LoadBook();

        //write initial numbers to the file
        String data = "a\n" +
                "ako:0501234567\n" +
                "argin:0541234567\n" +
                "b\n" +
                "bobo:0521234567\n" +
                "bibi:0531234567";
        writeBook(data);
        reloadBookFromFile();
    }

    public TreeMap<String, String> getBookByAlphabet(char c) {
        if(orderBooks.containsKey(String.valueOf(c))) {
            return orderBooks.get(String.valueOf(c));
        }
        return null;
    }

    public void addNewNumber(String name, String phone) {
        TreeMap<String, String> tree = getBookByAlphabet(name.charAt(0));
        if(tree != null) {
            tree.put(name, phone);
        } else {
            tree = new TreeMap<>();
            tree.put(name, phone);
            //TODO change big letter to small letter
            orderBooks.put(String.valueOf(name.charAt(0)), tree);
        }
        changeFlag = true;
    }

    public String searchNum(String name) {
        TreeMap<String, String> tree = getBookByAlphabet(name.charAt(0));
        if(tree != null && tree.containsKey(name)) {
            return tree.get(name);
        }
        return "";
    }

    public void updateNumber(String name, String number) {
        TreeMap<String, String> tree = getBookByAlphabet(name.charAt(0));
        if(tree != null && tree.containsKey(name)) {
            tree.put(name, number);
            changeFlag = true;
        }
    }

    public void deleteNumber(String name) {
        TreeMap<String, String> tree = getBookByAlphabet(name.charAt(0));
        tree.remove(name);
        if(tree.isEmpty()) {
            orderBooks.remove(String.valueOf(name.charAt(0)));
        }
        changeFlag = true;
    }

    private void writeBook(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileContext.openFileOutput(bookFile.getName(), Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
        changeFlag = false;
    }

    public void writeBookToFile() {

        //if there is not change, no need to write.
        if(!changeFlag) {
            return;
        }

        StringBuilder bookString = new StringBuilder();
        TreeMap<String, String> tree;

        //create string from the hash
        for(Map.Entry<String, TreeMap<String, String>> i : orderBooks.entrySet()) {
            bookString.append(i.getKey());
            bookString.append("\n");
            tree = i.getValue();
            for(Map.Entry<String, String> j : tree.entrySet()) {
                bookString.append(j.getKey());
                bookString.append(":");
                bookString.append(j.getValue());
                bookString.append("\n");
            }
        }

        writeBook(bookString.toString());
    }

    public void reloadBookFromFile() {

        orderBooks = new HashMap<>();

        try {
            InputStream inputStream = fileContext.openFileInput(bookFile.getName());

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString;

                TreeMap<String, String> tree = new TreeMap<>();

                //for each line
                while ( (receiveString = bufferedReader.readLine()) != null ) {

                    String[] lineSplit = receiveString.split(":");
                    //if it's a new letter, create new tree and add to hash
                    if(lineSplit.length == 1) {
                        tree = new TreeMap<>();
                        orderBooks.put(receiveString, tree);
                    } else {
                        tree.put(lineSplit[0], lineSplit[1]);
                    }
                }

                inputStream.close();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("main activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("main activity", "Can not read file: " + e.toString());
        }

    }
}
